Build Dependency For Krapture
=============================

Toolkit
-------

This project is a C++11 project.
The fallowing toolkits are tested on each platforms.

Microsoft Windows
^^^^^^^^^^^^^^^^^

`Microsoft Visual C++`_ >= 2010

GNU/Linux and Mac OS X
^^^^^^^^^^^^^^^^^^^^^^

`GNU Compiler Collection`_ >= 4.6

External Tools
--------------

`CMake`_ >= 2.6

Libraries
---------

`Qt toolkit`_ >= 4.7

Runtime Dependency For Krapture
===============================

It needs **SikuliServer** to work.
The path to **SikuliServer** is configurable.
For more information, please see the fallowing secions.

Build Dependency For SikuliServer
=================================

Toolkit
-------

Unfortunately, this is a Java project.
You need `Eclipse`_ to open it.

Install the fallowing libraries, then add them to build path in the Eclipse project.

Libraries
---------

* `Sikuli X`_ >= 1.0, as User Library named **Sikuli**
* `Gson`_, as User Library named **Gson**
* `Commons Codec`_, as User Library named **Commons Codec**

GNU/Linux
^^^^^^^^^

If you are using packages from distributions, you may also need to add `Jython`_ library.
This is because package maintainers usually prefer to split them to different packages.

Runtime Dependency For SikuliServer
===================================

**Sikuli X** is developed and only tested with `Java Runtime Environment`_ 6.
JRE 7 has known issues and may not work properly.

Microsoft Windows
-----------------

**Sikuli X** only provides 32-bits JNI binding.
This means so far you must use 32-bits JVM.


.. _Commons Codec: http://commons.apache.org/codec/
.. _CMake: http://www.cmake.org/
.. _Eclipse: http://www.eclipse.org/
.. _GNU Compiler Collection: http://gcc.gnu.org/
.. _Gson: http://code.google.com/p/google-gson/
.. _Java Runtime Environment: http://www.oracle.com/technetwork/java/javase/downloads/index.html
.. _Jython: http://www.jython.org/
.. _Microsoft Visual C++: http://msdn.microsoft.com/en-us/vstudio/aa718325
.. _Qt toolkit: http://qt.nokia.com/
.. _Sikuli X: http://sikuli.org/
