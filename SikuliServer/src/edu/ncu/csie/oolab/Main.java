/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package edu.ncu.csie.oolab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class Main {

	/**
	 * @param args
	 */
	public static void main( String[] args ) {
		for( Integer i = 0; i < args.length; ++i ) {
			if( args[i].equals( "--quite" ) ) {
				CommandParser.setVerbose( false );
			}
		}

		InputStreamReader reader = null;
		reader = new InputStreamReader( System.in, Charset.forName( "UTF-8" ) );
		BufferedReader cin = new BufferedReader( reader );

		CommandParser parser = new CommandParser();

		System.out.println( "ready" );

		while( true ) {
			String line = null;
			try {
				line = cin.readLine();
			} catch( IOException e ) {
				e.printStackTrace();
				break;
			}
			if( line == null ) {
				// EOF
				break;
			}
			line = parser.execute( line.trim() );
			if( line.equals( "bnVsbA==" ) ) {
				// null object in base64
				break;
			}
			System.out.println( line );
		}
	}

}
