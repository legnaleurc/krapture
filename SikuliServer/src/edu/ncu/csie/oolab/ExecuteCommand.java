/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package edu.ncu.csie.oolab;

import java.util.HashMap;

import org.python.core.PyException;
import org.python.util.PythonInterpreter;
import org.sikuli.script.FindFailed;

public class ExecuteCommand extends AbstractCommand {

	public ExecuteCommand( PythonInterpreter python ) {
		this.python_ = python;
	}

	@Override
	public HashMap< String, Object > execute( HashMap< String, Object > args ) {
		HashMap< String, Object > result = new HashMap< String, Object >();
		String line = ( String ) args.get( "script" );
		try {
			this.python_.exec( line );
		} catch( PyException e ) {
			result.put( "success", false );

			Object type = e.type.__tojava__( Class.class );
			if( type.equals( FindFailed.class ) ) {
				FindFailed ff = ( FindFailed ) e.value.__tojava__( FindFailed.class );
				result.put( "type", "FindFailed" );
				result.put( "message", ff.getMessage() );
			} else {
				result.put( "type", "Unknown" );
				result.put( "message", e.value.toString() );
			}

			return result;
		}
		result.put( "success", true );
		return result;
	}

	private PythonInterpreter python_;

}
