/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package edu.ncu.csie.oolab;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.apache.commons.codec.binary.Base64;
import org.python.util.PythonInterpreter;
import org.sikuli.script.Screen;
import org.sikuli.script.Settings;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CommandParser {

	public CommandParser() {
		this.HASH_TYPE = ( new TypeToken< HashMap< String, Object > >() {
		} ).getType();
		this.CHARSET = Charset.forName( "UTF-8" );
		this.json_ = new Gson();
		this.commands_ = new HashMap< String, AbstractCommand >();
		this.python_ = new PythonInterpreter();
		this.python_.setOut( System.err );
		this.screen_ = new Screen();

		this.commands_.put( "exit", new ExitCommand() );
		this.commands_.put( "capture", new CaptureCommand( this.screen_ ) );
		this.commands_.put( "execute", new ExecuteCommand( this.python_ ) );
		this.commands_.put( "bundle", new BundleCommand( this.python_ ) );

		this.python_.exec( "from sikuli.Sikuli import *" );
	}

	/**
	 * Executes commands.
	 * 
	 * b64Data must has command field.
	 * 
	 * @param b64Data
	 *            Base64 encoded JSON string
	 * @return null if command is invalid; Base64 encoded JSON string otherwise.
	 */
	public String execute( String b64Data ) {
		String json = null;
		json = new String( Base64.decodeBase64( b64Data.getBytes( this.CHARSET ) ), this.CHARSET );

		HashMap< String, Object > args = this.json_.fromJson( json, this.HASH_TYPE );

		AbstractCommand cmd = this.commands_.get( args.get( "command" ) );
		if( cmd == null ) {
			return null;
		}
		HashMap< String, Object > result = cmd.execute( args );
		json = this.json_.toJson( result );
		b64Data = new String( Base64.encodeBase64( json.getBytes( this.CHARSET ) ), this.CHARSET );
		return b64Data;
	}

	static public void setVerbose( Boolean verbose ) {
		Settings.ActionLogs = verbose;
		Settings.DebugLogs = verbose;
		Settings.InfoLogs = verbose;
		Settings.ProfileLogs = verbose;
	}

	private final Type HASH_TYPE;
	private final Charset CHARSET;
	private Gson json_;
	private HashMap< String, AbstractCommand > commands_;
	private PythonInterpreter python_;
	private Screen screen_;

}
