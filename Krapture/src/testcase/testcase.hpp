/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef KRAPTURE_TESTCASE_HPP
#define KRAPTURE_TESTCASE_HPP

#include <QtCore/QDateTime>
#include <QtCore/QString>
#include <QtCore/QUrl>

#include <map>
#include <memory>

namespace krapture {

	class TestCase {
	public:
		TestCase();

		const QString & getCreator() const;
		const QString & getDescription() const;
		const QString & getHTML() const;
		const std::map< QString, QUrl > & getImageMapping() const;
		int getLevel() const;
		const QDateTime & getModifyTime() const;
		const QString & getName() const;
		const QString & getProgramOptions() const;
		const QString & getScript() const;
		bool isActivated() const;

		void addImageMapping( const QString & name, const QUrl & uri );
		void setActivated( bool activated );
		void setCreator( const QString & creator );
		void setDescription( const QString & description );
		void setHTML( const QString & html );
		void setImageMapping( const std::map< QString, QUrl > & mapping );
		void setLevel( int level );
		void setModifyTime( const QDateTime & timestamp );
		void setName( const QString & name );
		void setProgramOptions( const QString & options );
		void setScript( const QString & script );

	private:
		TestCase( const TestCase & );
		TestCase & operator =( const TestCase & );

		class Private;
		std::shared_ptr< Private > p_;
	};

}

#endif
