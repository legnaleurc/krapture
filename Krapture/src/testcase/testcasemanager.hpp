/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef KRAPTURE_TESTCASEMANAGER_HPP
#define KRAPTURE_TESTCASEMANAGER_HPP

#include <QtCore/QString>
#include <QtSql/QSqlQueryModel>

#include <map>
#include <memory>
#include <tuple>
#include <vector>

namespace krapture {

	class TestCase;

	class TestCaseManager : public QSqlQueryModel {
	public:
		static void initialize();
		static TestCaseManager & instance();
		
		void close();
		void create();
		bool isOpen() const;
		void open();

		void deleteByIndex( const QModelIndex & index );
		std::vector< std::tuple< QString, QString > > getActivated() const;
		QString getName( const QModelIndex & index ) const;
		std::shared_ptr< TestCase > load( const QString & name ) const;
		void save( std::shared_ptr< TestCase > testcase );

	private:
		TestCaseManager();
		virtual ~TestCaseManager();
		TestCaseManager( const TestCaseManager & );
		TestCaseManager & operator =( const TestCaseManager & );

		class Private;
		friend class Private;
		std::unique_ptr< Private > p_;
	};

}

#endif
