/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef KRAPTURE_SIKULICLIENT_PRIVATE_HPP
#define KRAPTURE_SIKULICLIENT_PRIVATE_HPP

#include "sikuliclient.hpp"

#include <QtCore/QProcess>
#include <QtCore/QTextStream>
#include <QtCore/QTimer>
#include <QtScript/QScriptEngine>

#include <functional>

namespace krapture {

	class SikuliClient::Private : public QObject {
		Q_OBJECT

	public:
		typedef std::function< void () > Command;
		typedef std::vector< std::tuple< QString, QString > > BundleList;
		Private();

		void onActionTimeout( const QString & );
		void onActivationTimeout();
		void readBundle();
		void readCapture();
		void readExecute();
		void readReadyToken();
		void nextBundle();

		QVariantMap decode( const QString & );
		QString encode( const QScriptValue & );

		Command doFailure;
		Command doSuccess;
		bool opening;
		QProcess * server;
		QProcess * client;
		QScriptEngine * engine;
		QTextStream sio;
		QTimer * timer;
		QString clientPath;
		QString clientWD;
		BundleList bundles;
		BundleList::size_type currentBundle;

	public slots:
		void onFinished( int, QProcess::ExitStatus );
		void onReadyRead();
		void onReadyReadStandardError();
		void onTimeout();

	signals:
		void bundlesExecuted();
		void captured( const QString & );
		void error( const QString & );
		void executed( bool, const QString & );
		void log( const QString & );
		void ready();
		void taskCompleted( const QString &, bool, const QString & );
	};

}

#endif
