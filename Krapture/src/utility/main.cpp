/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "widget/mainwindow.hpp"
#include "settings.hpp"
#include "testcase/testcasemanager.hpp"
#include "krapture/config.h"

#include <QtGui/QApplication>
#include <QtCore/QTextCodec>

#define KRAPTURE_STRINGIZE_(x) #x
#define KRAPTURE_STRINGIZE(x) KRAPTURE_STRINGIZE_(x)

int main( int argc, char * argv[] ) {
	QApplication app( argc, argv );

	QApplication::setApplicationName( "Krapture" );
	QApplication::setApplicationVersion( KRAPTURE_STRINGIZE( KRAPTURE_VERSION ) );
	QApplication::setOrganizationDomain( "tw.edu.ncu" );
	QApplication::setOrganizationName( "oolab" );
	QTextCodec::setCodecForCStrings( QTextCodec::codecForName( "UTF-8" ) );
	QTextCodec::setCodecForTr( QTextCodec::codecForName( "UTF-8" ) );

	krapture::Settings::initialize();
	krapture::TestCaseManager::initialize();

	krapture::MainWindow window;
	window.show();
	window.reopenTestCaseManager();
	window.restartSikuliServer();

	return app.exec();
}
