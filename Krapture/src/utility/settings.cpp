/*
Copyright (c) 2011 Wei Cheng Pan <legnaleurc@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "settingsprivate.hpp"
#include "error/internalerror.hpp"

#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QReadLocker>
#include <QtCore/QSettings>
#include <QtCore/QTextCodec>
#include <QtCore/QWriteLocker>
#include <QtGui/QFont>

using namespace krapture;
using krapture::error::InternalError;

std::shared_ptr< Settings > Settings::Private::self;

void Settings::Private::destroy( Settings * self ) {
	delete self;
}

Settings::Private::Private(): QObject(), path(), table(), lock() {
	QDir appDir( QCoreApplication::instance()->applicationDirPath() );
	this->path = appDir.filePath( "krapture.ini" );
	QSettings s( this->path, QSettings::IniFormat );
	s.setIniCodec( QTextCodec::codecForName( "UTF-8" ) );

	this->table.insert( "capture_waiting", s.value( "capture_waiting", 2000 ) );
	this->table.insert( "font", s.value( "font", QFont( "Lucida Console", 12 ) ) );
	this->table.insert( "testing_program_path", s.value( "testing_program_path", QString() ) );
	this->table.insert( "sikuliserver_path", s.value( "sikuliserver_path", appDir.filePath( "SikuliServer.jar" ) ) );
	this->table.insert( "test_case_interval", s.value( "test_case_interval", 1500 ) );
	this->table.insert( "test_cases_path", s.value( "test_cases_path", appDir.filePath( "TestCases" ) ) );

	this->connect( QCoreApplication::instance(), SIGNAL( aboutToQuit() ), SLOT( save() ) );
}

/**
 * @brief Save settings from cache table
 * @note This slot is necessary because the destructing role of Singleton is complicate.
 */
void Settings::Private::save() {
	QSettings s( this->path, QSettings::IniFormat );
	s.setIniCodec( QTextCodec::codecForName( "UTF-8" ) );

	for( QVariantMap::const_iterator it = this->table.begin(); it != this->table.end(); ++it ) {
		switch( it.value().type() ) {
		case QVariant::Font:
			s.setValue( it.key(), it.value().toString() );
			break;
		default:
			s.setValue( it.key(), it.value() );
		}
	}
}

void Settings::initialize() {
	if( Settings::Private::self ) {
		return;
	}
	Settings::Private::self.reset( new Settings, Settings::Private::destroy );
}

/**
 * @brief Get global instance
 * @warning This function IS NOT thread-safe
 */
Settings & Settings::instance() {
	if( !Settings::Private::self ) {
		throw InternalError( "Settings has not initialized yet" );
	}
	return *Settings::Private::self;
}

Settings::Settings(): p_( new Private ) {
}

Settings::~Settings() {
}

QVariant Settings::get( const QString & key ) const {
	QReadLocker locker( &this->p_->lock );

	QVariantMap::const_iterator it = this->p_->table.find( key );
	if( it == this->p_->table.end() ) {
		return QVariant();
	}
	return it.value();
}

void Settings::set( const QString & key, const QVariant & value ) {
	QWriteLocker locker( &this->p_->lock );

	if( key.isEmpty() || !value.isValid() ) {
		return;
	}

	QVariantMap::iterator it = this->p_->table.find( key );
	if( it == this->p_->table.end() ) {
		this->p_->table.insert( key, value );
	} else {
		it.value() = value;
	}

	QSettings s( this->p_->path, QSettings::IniFormat );
	s.setIniCodec( QTextCodec::codecForName( "UTF-8" ) );
	s.setValue( key, value );
}
